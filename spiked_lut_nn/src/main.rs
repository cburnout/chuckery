mod lut;
mod lut_connection;
fn main() {
    let mut l = lut::Lut::new(3);
    let mut k = Vec::new();
    k.push(1);
    k.push(0);
    k.push(1);
    let mut v = Vec::new();
    v.push(0);
    v.push(1);
    v.push(1);
    l.insert(k, v);
    l.set_input_bit_val(0, 1);
    l.print();
    println!("output bit {:?}", l.get_output_bit(0));
}
